import { useSelector } from 'react-redux';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Rating from '@mui/material/Rating';
import { AccessTime } from '@mui/icons-material';
import { Button, createTheme, ThemeProvider } from '@mui/material';
import Link from 'next/link';
import Increment from './Increment';

const theme = createTheme({
  components: {
    MuiTypography: {
      variants: [
        {
          props: {
            variant: 'body2',
          },
          style: {
            fontSize: 11,
          },
        },
        {
          props: {
            variant: 'body2',
          },
          style: {
            fontSize: 9,
          },
        },
      ],
    },
  },
});

export interface Detail {
  id: number;
  name: string;
  duration: number;
  rating: number;
  numberOfReviews: number;
  price: number;
  image: number;
}

const TourCard: React.FC<{ detail: Detail }> = ({ detail }) => {
  const count = useSelector((state) => state.counter.value);
  return (
    <Grid item xs={12} sm={6} md={3}>
      <ThemeProvider theme={theme}>
        <Link href="/product/oneobject">
          <Paper elevation={3}>
            <img
              src={detail.image}
              alt=""
              className="img"
              height={230}
              width={257}
            />
            <Box paddingX={1}>
              <Typography variant="subtitle1" component="h2">
                {detail.name}
              </Typography>
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'center',
                }}
              >
                <AccessTime sx={{ width: 12.5 }} />
                <Typography variant="body2" component="p" marginLeft={0.5}>
                  {detail.duration} hours
                </Typography>
              </Box>
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'center',
                }}
                marginTop={3}
              >
                <Rating
                  name="read-only"
                  value={detail.rating}
                  readOnly
                  precision={0.5}
                  size="small"
                />
                <Typography variant="body2" component="p" marginLeft={0.5}>
                  {detail.rating}
                </Typography>
                <Typography variant="body2" component="p" marginLeft={1.5}>
                  ({detail.numberOfReviews} reviews)
                </Typography>
              </Box>
              <Box>
                <Typography variant="h6" component="h3" marginTop={0}>
                  ${detail.price}
                </Typography>
              </Box>
              <Box>
                <Typography variant="h6" component="h3" marginTop={0}>
                  <Increment />
                </Typography>
              </Box>
            </Box>
          </Paper>
        </Link>
      </ThemeProvider>
    </Grid>
  );
};

export default TourCard;
