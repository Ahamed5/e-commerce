import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { ShoppingCart } from '@mui/icons-material';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import DeleteIcon from '@mui/icons-material/Delete';
import Link from 'next/link';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { Badge } from '@mui/material';

export default function SearchAppBar() {
  const count = useSelector((state) => state.counter.value);
  const [open, setOpen] = useState(false);

  const [cart, setCart] = useState(0);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Professional Camera Drone</DialogTitle>
        <hr color="#b2bec3" />
        <DialogContent>
          <Box sx={{ display: 'flex' }}>
            <img
              src="https://ik.imagekit.io/lcv/techne/deals/1093/overview/4d7f7066-b5e3-474d-9bc2-516b88d52f45.jpg?tr=w-2800"
              alt=""
              height={175}
              width={250}
            />
          </Box>
        </DialogContent>

        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Button sx={{ color: 'Green' }} onClick={() => setCart(cart - 1)}>
            <RemoveCircleIcon />
          </Button>
          <Typography sx={{ color: 'blue' }}>{cart}</Typography>
          <Button sx={{ color: 'Green' }} onClick={() => setCart(cart + 1)}>
            <AddCircleIcon />
          </Button>
          <Typography
            variant="h6"
            component="h3"
            marginTop={0}
            sx={{ color: '#191919' }}
          >
            $98
          </Typography>
          <Button sx={{ color: 'Red' }}>
            <DeleteIcon />
          </Button>
        </Box>
        <hr color="#b2bec3" />

        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
        </DialogActions>
      </Dialog>

      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static" sx={{ bgcolor: '#191919' }}>
          <Toolbar>
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{
                flexGrow: 1,
                display: { xs: 'none', sm: 'block' },
              }}
            >
              Flipkart
            </Typography>

            <Link href="">
              <Typography variant="h6" component="h4" marginRight={10}>
                Home
              </Typography>
            </Link>

            <Link href="/product/oneobject">
              <Typography variant="h6" component="h4" marginRight={10}>
                Product
              </Typography>
            </Link>

            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="open drawer"
              sx={{ mr: 2 }}
              onClick={handleClickOpen}
            >
              <Badge badgeContent={count} color="primary">
                <ShoppingCart />
              </Badge>
            </IconButton>
          </Toolbar>
        </AppBar>
      </Box>
    </>
  );
}
