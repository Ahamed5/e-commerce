import { Button, Typography } from '@mui/material';
import React from 'react';
import { useDispatch } from 'react-redux';
import { increment } from '../components/counterSlice';

function Increment() {
  const dispatch = useDispatch();

  return (
    <div>
      <Typography>
        <Button onClick={() => dispatch(increment())}>Add to Cart</Button>
      </Typography>
    </div>
  );
}

export default Increment;
