import TourCard from '../components/TourCard';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import SearchAppBar from '../components/AppBar';
import products from '../data.json';
import { Typography } from '@mui/material';

const Landing = () => (
  <div className="Home">
    <SearchAppBar />
    <Container sx={{ marginY: 5 }}>
      {products.map((product) => (
        <>
          <Typography
            variant="h4"
            component="h2"
            marginTop={5}
            marginBottom={3}
          >
            {product.name} Products
          </Typography>
          <Grid container spacing={5}>
            {product.details.map((detail, index) => (
              <TourCard detail={detail} key={index} />
            ))}
          </Grid>
        </>
      ))}
    </Container>
  </div>
);

export default Landing;
