import { useSelector } from 'react-redux';
import Decrement from '../../components/Decrement';
import Increment from '../../components/Increment';

function Cart() {
  const count = useSelector((state) => state.counter.value);
  return (
    <div>
      <Increment />
      <Decrement />
      {count}
    </div>
  );
}

export default Cart;
