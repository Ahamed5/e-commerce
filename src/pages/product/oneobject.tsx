import { Typography } from '@mui/material';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import ImageCollage from '../../components/imageCollage';
import SearchAppBar from '../../components/AppBar';
import Rating from '@mui/material/Rating';
import { AccessTime } from '@mui/icons-material';
import { Button } from '@mui/material';
import React, { useState } from 'react';
import Image from 'next/image';
import Increment from '../../components/Increment';

const Oneobject = () => {
  const [cart, setCart] = useState(0);

  return (
    <>
      <SearchAppBar />
      <Container sx={{ width: 900 }}>
        <Typography variant="h4" component="h2" marginTop={3}>
          Professional Long Range Distance With HD Camera Drone
        </Typography>

        <Grid item xs={12} sm={6} md={3}>
          <Paper elevation={3}>
            <Box marginTop={3} sx={{ display: 'flex' }}>
              <img
                src="https://ik.imagekit.io/lcv/techne/deals/1093/overview/4d7f7066-b5e3-474d-9bc2-516b88d52f45.jpg?tr=w-2800"
                alt=""
                height={325}
              />
              <ImageCollage />
              <Box paddingX={1}>
                <Typography variant="subtitle1" component="h2">
                  Camera Drone
                </Typography>
                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                  }}
                >
                  <AccessTime sx={{ width: 12.5 }} />
                  <Typography variant="body2" component="p" marginLeft={0.5}>
                    5 hours
                  </Typography>
                </Box>
                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                  }}
                  marginTop={3}
                >
                  <Rating
                    name="read-only"
                    readOnly
                    precision={0.5}
                    size="small"
                  />
                  <Typography variant="body2" component="p" marginLeft={0.5}>
                    4.5
                  </Typography>
                </Box>

                <Typography variant="h6" component="h3" marginTop={0}>
                  $98
                </Typography>
                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'flex',
                  }}
                  marginBottom={0}
                >
                  <Typography variant="body2" component="h3" marginBottom={0}>
                    <Button
                      variant="outlined"
                      onClick={() => setCart(cart + 1)}
                    >
                      <Increment />
                    </Button>
                  </Typography>
                </Box>
              </Box>
            </Box>
          </Paper>
        </Grid>
      </Container>
    </>
  );
};

export default Oneobject;
